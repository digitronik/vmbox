import os

import click
import requests
from bs4 import BeautifulSoup
from ruamel.yaml import safe_load

from vmbox.config import Configuration


class Repositories(object):
    def __init__(self, repos_file="vmbox/repositories.yaml"):
        with open(repos_file, "r") as f:
            self.repos = safe_load(f)

    @property
    def distros(self):
        return self.repos.get("distros")


class LinuxDistro(Repositories):
    def __init__(self, name):
        self.name = name
        Repositories.__init__(self)

    @property
    def data(self):
        return self.distros.get(self.name)

    @property
    def releases(self):
        return list(map(str, self.data.get("releases")))

    @property
    def repo_link(self):
        return self.data.get("repo")

    @property
    def img_format(self):
        return self.data.get("format")

    def release_url(self, release=None):
        if not release:
            release = self.releases[-1]
        return self.data.get("url").format(
            repo=self.repo_link, rel=release
        )

    def images(self, release=None, release_url=None, ssl_verify=False):
        """All available cloud remote images"""
        if not release_url:
            release_url = self.release_url(release=release)
        page = requests.get(release_url, verify=ssl_verify).text
        soup = BeautifulSoup(page, "html.parser")
        return [
            node.get("href")
            for node in soup.find_all("a")
            if node.get("href").endswith(self.img_format)
        ]

    def download(self, image, release=None, path=None):
        """Download image with click progress bar
        Args:
            url: cloud image url
            release: release number
            path: Image download to local path
        """
        if not path:
            path = os.path.join(os.environ["HOME"], "Downloads")

        click.echo("Download request for: {img}".format(img=image))
        url = os.path.join(self.release_url(release=release), image)
        r = requests.get(url, stream=True)

        if r.status_code != requests.codes.ok:
            click.echo("Unable to connect {url}".format(url=url))
            r.raise_for_status()

        total_size = int(r.headers.get("Content-Length"))
        local_img_path = os.path.join(path, image)

        with click.progressbar(
            r.iter_content(1024), length=total_size
        ) as bar, open(local_img_path, "wb") as file:
            for chunk in bar:
                file.write(chunk)
                bar.update(len(chunk))


@click.command(help="Pull Image")
@click.option(
    "-l", "--local", is_flag=True, help="All available local images"
)
@click.option(
    "-r", "--remote", is_flag=True, help="All available remote images"
)
def images(local, remote):
    """Get local or remote available image
    Args:
        local: default, will give local images
        remote: will return remote repository available images
    """
    repos = Repositories()

    if remote:
        distro_name = click.prompt(
            "Linux distribution:",
            default="cirros",
            type=click.Choice(repos.distros.keys()),
        )
        distro = LinuxDistro(distro_name)
        release = click.prompt(
            "Release:",
            default=distro.releases[-1],
            type=click.Choice(distro.releases),
        )
        for img in distro.images(release=release):
            click.echo(img)
    else:
        img_dir = Configuration().data.get("images")
        for img in os.listdir(img_dir):
            click.echo(img)


@click.command(help="Download Image")
@click.argument("name")
def pull(name):
    """Pull image available on remote repository
    Note: Image name can found by images -r command

    Args:
        name: (`str`) image name
    """
    distro_info = name.split("-")
    distro_name = distro_info[0].lower()

    if distro_name == "fedora":
        ver = distro_info[3]
    else:
        ver = distro_info[1]

    distro = LinuxDistro(distro_name)
    conf = Configuration()
    distro.download(
        image=name, release=ver, path=conf.data.get("images")
    )


@click.command(help="Remove Image")
@click.argument("name")
def rmi(name):
    """Remove local image

    Args:
        name: local image name
    """
    img_dir = Configuration().data.get("images")

    if name in os.listdir(img_dir):
        os.system(
            "rm -rf '{img_dir}/{name}'".format(
                img_dir=img_dir, name=name
            )
        )
    else:
        click.echo("{img} not available".format(img=name))
