import os

import click
from ruamel.yaml import safe_dump, safe_load

HOME = os.environ["HOME"]


class Configuration(object):
    """Configure vmbox"""

    def __init__(self):
        self.conf_file = "{home}/.config/vmbox/conf.yaml".format(
            home=HOME
        )
        dir_path = os.path.dirname(self.conf_file)

        if not os.path.isdir(dir_path):
            os.makedirs(dir_path)

        if not os.path.isfile(self.conf_file):
            raw_cfg = {
                "hypervisor": "qemu:///system",
                "pool_name": "vmbox",
                "pool": "{home}/.vmbox/pool".format(home=HOME),
                "images": "{home}/.vmbox/images".format(home=HOME),
            }
            self.write(raw_cfg)

    def read(self):
        with open(self.conf_file, "r") as ymlfile:
            return safe_load(ymlfile)

    def write(self, cfg):
        if not os.path.isdir(cfg.get("pool")):
            os.makedirs(cfg.get("pool"), exist_ok=True)
        if not os.path.isdir(cfg.get("images")):
            os.makedirs(cfg.get("images"), exist_ok=True)

        with open(self.conf_file, "w") as ymlfile:
            return safe_dump(cfg, ymlfile, default_flow_style=False)

    @property
    def data(self):
        return self.read()


@click.command(help="Configure miqbox")
def config():
    conf = Configuration()
    cfg = conf.read()

    cfg["hypervisor"] = click.prompt(
        "Hypervisor drivers url", default=cfg.get("hypervisor")
    )
    cfg["pool_name"] = click.prompt(
        "Storage Pool Name", default=cfg.get("pool_name")
    )
    cfg["pool"] = click.prompt(
        "Storage Pool Path", default=cfg.get("pool")
    )
    cfg["images"] = click.prompt(
        "Local Image Location", default=cfg.get("images")
    )

    conf.write(cfg=cfg)
    click.echo("Configuration saved successfully...")
