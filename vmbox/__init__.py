import click
from vmbox.config import Configuration, config
from vmbox.images import images, pull, rmi
from vmbox.vm import create, status, stop, start


@click.version_option()
@click.group()
def main():
    Configuration()


# commands

# Image
main.add_command(images)
main.add_command(pull)
main.add_command(rmi)

# Config
main.add_command(config)

# VM
main.add_command(status)
main.add_command(start)
main.add_command(stop)
main.add_command(create)


if __name__ == "__main__":
    main()
