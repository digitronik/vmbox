import os

import click
import libvirt

from vmbox.config import Configuration
from vmbox.xml import POOL

VM_STATE = {
    libvirt.VIR_DOMAIN_RUNNING: "running",
    libvirt.VIR_DOMAIN_BLOCKED: "idle",
    libvirt.VIR_DOMAIN_PAUSED: "paused",
    libvirt.VIR_DOMAIN_SHUTDOWN: "in shutdown",
    libvirt.VIR_DOMAIN_SHUTOFF: "shut off",
    libvirt.VIR_DOMAIN_CRASHED: "crashed",
    libvirt.VIR_DOMAIN_NOSTATE: "no state",
}


class VMBox(object):
    def __init__(
        self,
        hypervisor=None,
        pool_name=None,
        pool_path=None,
        images_path=None,
    ):
        conf = Configuration().data

        self.pool_name = (
            pool_name if pool_name else conf.get("pool_name")
        )
        self.pool_path = pool_path if pool_path else conf.get("pool")
        self.images_path = (
            images_path if images_path else conf.get("images")
        )

        hypervisor = (
            hypervisor if hypervisor else conf.get("hypervisor")
        )
        try:
            self.conn = libvirt.open(hypervisor)
        except Exception:
            click.echo(
                "Failed to open connection to {}".format(hypervisor)
            )
            exit(1)

    @property
    def local_images(self):
        return os.listdir(self.images_path)

    def vms(self, status=None):
        """List VMs as per current status
        Args:
            status: (`str`) running, shut off, paused,
                    idle, crashed, no state
        Returns:
            (`dirt`) all appliances/ appliances are per status
        """
        domains = {
            domain.name(): domain
            for domain in self.conn.listAllDomains()
        }

        if status:
            return {
                name: dom
                for name, dom in domains.items()
                if VM_STATE[dom.state()[0]] == status
            }
        else:
            return domains

    @property
    def list_vms(self):
        return list(self.vms().keys())

    def pool(self):
        try:
            return self.conn.storagePoolLookupByName(self.pool_name)
        except Exception:
            return None

    def create_pool(self, autostart=True, active=True):
        """Create storage pool."""

        pool_xml = POOL.format(name=self.pool_name, path=self.pool_path)
        pool = self.conn.storagePoolDefineXML(pool_xml, 0)

        if active and not pool.isActive():
            pool.create()

        if autostart and not pool.autostart():
            pool.setAutostart(autostart=True)

        return pool


class VM(object):
    def __init__(self, name=None, id=None, hypervisor=None):
        self.name = name
        self.id = id

        conf = Configuration().data
        hypervisor = (
            hypervisor if hypervisor else conf.get("hypervisor")
        )
        try:
            self.conn = libvirt.open(hypervisor)
        except Exception:
            click.echo(
                "Failed to open connection to {}".format(hypervisor)
            )
            exit(1)

    @property
    def domain(self):
        """Domain of VM"""
        try:
            if self.id:
                dom = self.conn.lookupByID(self.id)
            elif self.name:
                dom = self.conn.lookupByName(self.name)
            return dom
        except Exception:
            print("VM name not found")
            return None

    @property
    def status(self):
        return VM_STATE[self.domain.state()[0]]

    def start(self):
        """ Start/ Invoke VM"""
        if not self.domain.isActive():
            self.domain.create()
            return True
        else:
            return False

    def stop(self):
        """Stop VM"""
        if self.domain.isActive():
            self.domain.shutdown()
            return True
        else:
            return False

    def ips(self):
        """Collect all ips related to VM"""
        ips = dict()
        if self.domain.isActive():
            try:
                ifaces = self.domain.interfaceAddresses(
                    libvirt.VIR_DOMAIN_INTERFACE_ADDRESSES_SRC_LEASE, 0
                )
                for iface, address in ifaces.items():
                    for addrs in address["addrs"]:
                        if (
                            addrs["type"]
                            == libvirt.VIR_IP_ADDR_TYPE_IPV4
                        ):
                            ips[iface] = addrs["addr"]
            except Exception:
                pass
        return ips

    @property
    def hostname(self):
        """collect ipv4 hostname for ssh"""
        ips = self.ips()
        if ips:
            for conn, ip in ips.items():
                if "lo" == conn:
                    pass
                else:
                    return ip
            else:
                return None

    def info(self):
        id = self.domain.ID() if self.domain.ID() > 0 else "---"
        hostname = self.hostname if self.hostname else "---"

        return {
            "id": id,
            "name": self.domain.name(),
            "UUID": self.domain.UUIDString(),
            "state": self.status,
            "hostname": hostname,
        }


@click.command(help="VMBox all VM Status")
@click.option("-a", "--all", is_flag=True, help="All Appliances")
@click.option(
    "-r", "--running", is_flag=True, help="All Running Appliances"
)
@click.option(
    "-s", "--stop", is_flag=True, help="All Stopped Appliances"
)
def status(all, running, stop):
    """Get appliances status
    Args:
        all: default will return all appliances status
        running: will return all running appliances status
        stop: will return all stop appliances status
    Returns:
        echo status on cli
    """
    if running:
        status = "running"
    elif stop:
        status = "shut off"
    else:
        status = None

    box = VMBox()
    data = [
        VM(name=name).info() for name in box.vms(status=status).keys()
    ]

    for index, info in enumerate(data):
        if not index:
            click.echo(
                "{:<5s}{:<20s}{:^10s}{:^15s}".format(
                    "Id", "Name", "Status", "Hostname"
                )
            )
        click.echo(
            "{:<5s}{:<20s}{:^10s}{:^15s}".format(
                str(info["id"]),
                info["name"],
                info["state"],
                info["hostname"],
            )
        )


@click.command(help="Start VM")
@click.argument("name", type=click.STRING)
def start(name):
    """ Start/ Invoke VM
    Args:
        name: (`str`) VM name
    """
    vm = VM(name=name)
    if not vm.start():
        click.echo("Fail to start {}".format(name))


@click.command(help="Stop VM")
@click.argument("name")
def stop(name):
    """Stop running VM
    Args:
        name: (`str`) VM name
        id: (`int`) VM ID
    """
    try:
        id = int(name)
        vm = VM(id=id)
    except ValueError:
        vm = VM(name=name)

    if not vm.stop():
        click.echo("Fail to stop {}".format(name))


@click.command(help="Create Appliance")
@click.option("--name", prompt="Appliance name")
@click.option("--image", prompt="Image name")
@click.option("--cpu", default=1, prompt="CPU count")
@click.option("--memory", default=4, prompt="Memory in GiB")
def create(name, image, cpu, memory):
    """Create appliance
    Args:
        name: VM name
        image: Cloud image
        cpu: CPU count
        memory: Memory
    """
    extension = image.split(".")[-1]
    base_disk_name = "{name}.{ext}".format(name=name, ext=extension)

    box = VMBox()
    if image in box.local_images:
        source = os.path.join(box.images_path, image)
        destination = os.path.join(box.pool_path, base_disk_name)
        os.system(
            "sudo cp {source} {destination}".format(
                source=source, destination=destination
            )
        )
        click.echo("Base VM disk created...")
    else:
        click.echo("'{img}' locally not available...".format(img=image))
        exit(0)
